import './App.css';
import Web3Modal from "web3modal";
import { providers, ethers, Contract } from 'ethers'; 
import { useState, useEffect, useRef } from 'react';
import { DODGERINU_ADDRESS, abi } from './helpers/constants';
import { Toaster, toast } from 'react-hot-toast';

function App() {

  const [walletConnected, setWalletConnected] = useState(false);
  const [walletAddress, setWalletAddress] = useState('');
  const [accountBalance, setAccountBalance] = useState(0);
  const [transferTo, setTransferTo] = useState('');
  const [transferAmount, setTransferAmount] = useState(0);
  const [sending, setSending] = useState(false);

  const web3ModalRef = useRef();

  const getProviderOrSigner = async (needSigner = false) => {
    const provider = await web3ModalRef.current.connect();
    const Web3Provider = new ethers.providers.Web3Provider(provider);

    const {chainId} = await Web3Provider.getNetwork();
    if(chainId !== 11155111) {
      alert('Please connect to the sepolia network');
      throw new Error('Please connect to the sepolia network');
    }

    if(needSigner) {
      return Web3Provider.getSigner();
    }

    return Web3Provider;
  }

  const handleConnect = async (e) => { 
    e.preventDefault();
    try {
      const signer = await getProviderOrSigner(true);
      const address = await signer.getAddress();
      setWalletConnected(true);
      setWalletAddress(address);
      getAccountBalance(address);
    } catch (error) {
      console.log(error);
      toast.error('Error connecting wallet');
    }
  };

  const handleDisconnect = async (e) => { 
    e.preventDefault();
    try {
      setWalletConnected(false);
      setWalletAddress('');
    } catch (error) {
      console.log(error);
      toast.error('Error disconnecting wallet');
    }
  };

  const getAccountBalance = async (address) => {
    try {
      const provider = await getProviderOrSigner();
      const tokenContract = new Contract(DODGERINU_ADDRESS, abi, provider);
      const _accountBalance = await tokenContract.balanceOf(address.toString());
      setAccountBalance(ethers.utils.formatEther(_accountBalance.toString()));
    } catch (error) {
      console.log(error);
      toast.error('Error getting account balance');
    }
  }

  const sendTokens = async (e) => { 
    e.preventDefault();

    if(parseInt(transferAmount) <= 0) {
      toast.error('Amount must be greater than 0');
      return;
    }

    if(transferTo === '') { 
      toast.error('Please enter a valid address');
      return;
    }

    const amountToTransfer = ethers.utils.parseEther(transferAmount.toString());

    try {
      const signer = await getProviderOrSigner(true);
      const tokenContract = new Contract(DODGERINU_ADDRESS, abi, signer);
      const transferStatus = await tokenContract.transfer(transferTo, amountToTransfer);
      setSending(true);
      await transferStatus.wait();
      setSending(false);
      const etherscanLink = `https://sepolia.etherscan.io/tx/${transferStatus.hash}`;
      toast.success((t) => (
        <p>Transaction successful! 
          check status here <a rel='noreffer' target='_blank' href={etherscanLink}>View block explorer</a>
        </p>
      ), {
        duration: 10000
      });
      getAccountBalance(walletAddress);
    }catch (error) {
      toast.error('Error sending tokens');
      return;
    }
  }

  useEffect(() => {
    if(!walletConnected) {
      web3ModalRef.current = new Web3Modal({
        network: "sepolia",
        providerOptions: {},
        disableInjectedProvider: false,
      });
    }

  }, []);
  return (
    <div className="App">
      <Toaster />
      <header className="App-header">
        <h3>
        Dodger Inu Token Wallet
        </h3>
        <div className='wrapperDiv'>
        {walletConnected ? ( 
          <>
           <b>Balance: </b> {accountBalance} DGI
           <form className='transferForm' onSubmit={(e) => sendTokens(e)}>
            <div className='formLeft'>
              <input type='text' name='toAddress' value={transferTo} onChange={(e) => setTransferTo(e.target.value)} placeholder='To Address'/>
              <input min='0' type='number' name='transferAmount' value={transferAmount} onChange={(e) => setTransferAmount(e.target.value)} placeholder='Amount'/>
            </div>
            <div className='formRight'>
              <button type='submit' > 
              {sending ? 'Sending...' : 'Send Token'}
              </button>
            </div>
           </form>
           <button className='button' style= {{'backgroundClip': '#e55039'}} onClick={(e) => handleDisconnect(e)}>
            Disconnect
           </button>
          </>
        ): (
        <>
          <p>Please connect your wallet</p>
          <button className='button' onClick={async (e) => handleConnect(e) }>Connect Wallet</button>
        </>)} 
        </div>
      </header>
    </div>
  );
}

export default App;
